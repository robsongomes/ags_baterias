<style>
    #row-one {
        margin-top: 90px;
    }
    .titulo {
        text-align: center;
    }
    #hrLinha {
        height: 500px;
        width: 3px;
        background-color: rgb(221, 221, 221);
    }        
    .grafico {
        margin-left: 5%;
    }
    .row-seguinte {
        margin-top: 30px;
    }
    .btn {
        margin-top: 6px;
    }
    
    p {
        margin-top: 25px;
        text-align: center;
        font-size: 15px !important;
    }
</style>

<div class="container">    
    <div id="row-one" class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 box-project animated bounceInLeft">
            <a href="~/ags_baterias/home/index" class="btn btn-primary btn-lg pull-right"><i class="fa fa-arrow-circle-right"></i> Acesse</a>
            <h1 class="titulo">AGS Baterias</h1>
            <canvas class="grafico hidden-xs hidden-sm" id="agsBaterias" width="1000" height="300"></canvas>            
            <p class="hidden-xs hidden-md">Montante de vendas pelos meses do ano</p>
        </div>                
    </div>
    <div class="row row-seguinte">
        <div class="col-xs-12 col-sm-12 col-md-12 box-project animated bounceInLeft">
            <a href="#" class="btn btn-primary btn-lg pull-right"><i class="fa fa-arrow-circle-right"></i> Acesse</a>
            <h1 class="titulo">AGS Lava Jato</h1>
            <canvas class="grafico hidden-xs hidden-sm" id="agsLavajato" width="1000" height="300"></canvas>  
            <p class="hidden-xs hidden-sm">Montante de vendas pelos meses do ano</p>
        </div>
    </div>
</div>
<script type="text/javascript">
            $(document).ready(function () {                                                                                                                                                                                               
                var data = {
                    labels: ["Jan", "Fev", "Mar", "Abr", "Maio", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
                    datasets: [
                        {
                            label: "Gráfico de vendas",
                            fillColor : "rgba(151,187,205,0.2)",
                            strokeColor : "rgba(151,187,205,1)",
                            pointColor : "rgba(151,187,205,1)",
                            pointStrokeColor : "#fff",
                            pointHighlightFill : "#fff",
                            pointHighlightStroke : "rgba(151,187,205,1)",
                            data: [65, 59, 80, 81, 56, 55, 40, 50, 45, 23, 34, 80]
                        }
                    ]
                };
                var c1 = document.getElementById("agsBaterias").getContext("2d");
                var c2 = document.getElementById("agsLavajato").getContext("2d");
                var agsBaterias = new Chart(c1).Line(data, {responsive: false});
                var agsLavajato = new Chart(c2).Line(data, {responsive: false});                                
            });            
        </script>