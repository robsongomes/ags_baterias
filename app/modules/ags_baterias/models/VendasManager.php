<?php

class VendasManager extends Manager {
    
    public static function getAllVendasADinheiro() {        
        try {            
            $db = Database::factory();
            $sql = "SELECT 
                    v.id,
                    v.quantidade,
                    v.valor,
                    v.desconto,
                    v.dataDaVenda,
                    v.cliente,
                    v.formaDePagamento,
                    u.nomeUsuario,
                    e.amperagem,
                    e.marca
                    FROM agsbaterias.venda v
                    INNER JOIN agsbaterias.usuario u
                    ON v.idUsuario = u.id 
                    INNER JOIN agsbaterias.estoque e
                    ON v.idEstoque = e.id
                    WHERE v.formaDePagamento = 'dinheiro'
                    ORDER BY v.dataDaVenda DESC";
            $result = $db->query($sql);        
            return $result;            
        } catch (Exception $ex) {
            throw new Exception('Erro ao recuperar os registros de vendas: '. $ex->getMessage());
        }    
    }
    
    public static function getAllVendasNoCartao() {        
        try {            
            $db = Database::factory();
            $sql = "SELECT 
                    v.id,
                    v.quantidade,
                    v.valor,
                    v.desconto,
                    v.dataDaVenda,
                    v.cliente,
                    v.formaDePagamento,
                    u.nomeUsuario,
                    e.amperagem,
                    e.marca
                    FROM agsbaterias.venda v
                    INNER JOIN agsbaterias.usuario u
                    ON v.idUsuario = u.id 
                    INNER JOIN agsbaterias.estoque e
                    ON v.idEstoque = e.id
                    WHERE v.formaDePagamento = 'cartao'
                    ORDER BY v.dataDaVenda DESC";
            $result = $db->query($sql);        
            return $result;            
        } catch (Exception $ex) {
            throw new Exception('Erro ao recuperar os registros de vendas: '. $ex->getMessage());
        }    
    }
    
    public static function insert($idEstoque, $qt, $valor, $desconto, $cliente, $formaDePagamento, $idUsuario) {
        try {             
            $db = self::getConection();
            $db->beginTransaction();
            $sql = "INSERT INTO agsbaterias.venda (idEstoque, quantidade, valor, desconto, cliente, formaDePagamento, idUsuario, dataDaVenda)
                   VALUES ('$idEstoque', '$qt', '$valor', '$desconto', '$cliente', '$formaDePagamento', '$idUsuario', now())";
            $query = $db->prepare($sql);
            $query->execute();
            $count1 = $query->rowCount();            
            
            $sql2 = "UPDATE agsbaterias.estoque SET quantidade = quantidade - '$qt', dataDaVenda = now() WHERE id = '$idEstoque'";
            $query2 = $db->prepare($sql2);
            $query2->execute();
            $count2 = $query2->rowCount();            
            if($count1 == 0 || $count2 == 0) {
                $db->rollBack();
                throw new Exception('Erro ao inserir a venda');
            } else {
                $db->commit();
            }
        } catch (Exception $ex) {
            throw new Exception('Erro ao inserir a venda: '. $ex->getMessage());
        } 
    }
}

