<?php

class EstoqueManager extends Manager {
    
    public static function getAll() {        
        try {                                             
            $sql = "SELECT * FROM agsbaterias.estoque WHERE quantidade > 0 ORDER BY marca";            
            $query = self::getConection()->prepare($sql);
            $query->execute();                        
            $result  = $query->fetchAll();            
            $result = self::getStd($result);            
            return $result;            
        } catch (Exception $ex) {
            throw new Exception('Erro ao recuperar os registros: '. $ex->getMessage());            
        }    
    }
    
    public static function getById($idEstoque) {
        try {            
            $db = Database::factory();
            $sql = "SELECT * FROM agsbaterias.estoque WHERE id = '$idEstoque' LIMIT 1";
            $result = $db->query($sql);        
            return $result[0];            
        } catch (Exception $ex) {
            throw new Exception('Erro ao recuperar o registro: '. $ex->getMessage());
        } 
    }
    
    public static function insert($marca, $amperagem, $quantidade, $valor) {
        try {                   
            $db = Database::factory();
            $sql = "SELECT id FROM agsbaterias.estoque
                    WHERE marca = '$marca'
                    AND amperagem = '$amperagem'
                    AND valor = '$valor'";
            $cont = $db->query($sql);
            if(isset($cont) && !empty($cont)) {
                $sql2  = "UPDATE agsbaterias.estoque SET quantidade = quantidade + '$quantidade'
                        WHERE marca = '$marca'
                        AND amperagem = '$amperagem' AND quantidade = '$quantidade' AND valor = '$valor'";
                $result = $db->query($sql2);
            } else {
                $sql3 = "INSERT INTO agsbaterias.estoque (marca, amperagem, quantidade, valor, dataDoCadastro)
                    VALUES('$marca', '$amperagem', '$quantidade', '$valor', NOW())";
                $result = $db->query($sql3);
            }                         
            return $result;            
        } catch (Exception $ex) {
            throw new Exception('Erro ao inserir o registro: '. $ex->getMessage());
        }  
    }
    
    public static function updateById($idEstoque, $marca, $amperagem, $quantidade, $valor) {
        try {                   
            $db = Database::factory();
            $sql = "UPDATE agsbaterias.estoque SET marca = '$marca', amperagem = '$amperagem', quantidade = '$quantidade', valor = '$valor', dataDoCadastro = NOW()
                    WHERE id = '$idEstoque'";
            $result = $db->query($sql);             
            return $result;            
        } catch (Exception $ex) {
            throw new Exception('Erro ao atualizar o registro: '. $ex->getMessage());
        } 
    }
    
    public static function deleteById($idEstoque) {
        try {                
            $db = self::getConection();
            $sql = "SELECT * FROM agsbaterias.venda WHERE idEstoque = '$idEstoque'";
            $query = $db->prepare($sql);
            $query->execute();
            $cont = $query->rowCount();            
            if($cont == 0) {                
                $sql2 = "DELETE FROM agsbaterias.estoque WHERE id = '$idEstoque'";
                $query = $db->prepare($sql2);
                $query->execute();
            } else {                
                throw new Exception('O estoque não pode ser exclúido! Ele já começou a ser vendido.');
            }                     
        } catch (Exception $ex) {
            throw new Exception('Erro ao excluir o registro: '. $ex->getMessage());
        }  
    }    
    
    /*
     * Querys do módulo de baterias deixadas como sucatas
     */
    
    public static function getAllSucatas() {
        try {                                             
            $sql = "SELECT * FROM agsbaterias.sucata ORDER BY marca";            
            $query = self::getConection()->prepare($sql);
            $query->execute();                        
            $result  = $query->fetchAll();            
            $result = self::getStd($result);            
            return $result;            
        } catch (Exception $ex) {
            throw new Exception('Erro ao recuperar os registros de sucata: '. $ex->getMessage());
        } 
    }
    
    public static function getSucataById($idSucata) {
        try {            
            $sql = "SELECT * FROM agsbaterias.sucata WHERE id = '$idSucata' LIMIT 1";            
            $query = self::getConection()->prepare($sql);
            $query->execute();                        
            $temp[]  = $query->fetch();                         
            $result = self::getStd($temp);       
            if(isset($result[0]) && !empty($result[0])) {
                return $result[0];
            } else {
                throw new Exception('Erro ao recuperar o registro');
            }            
        } catch (Exception $ex) {
            throw new Exception('Erro ao recuperar o registro: '. $ex->getMessage());
        } 
    }                    
    
    public static function deleteSucataById($idSucata) {
        try {                                             
            $sql = "DELETE FROM agsbaterias.sucata WHERE id = '$idSucata'";            
            $query = self::getConection()->prepare($sql);
            $query->execute();                                       
            $cont = $query->rowCount();
            if($cont == 0) {
                throw new Exception('Erro ao exluir o registro');
            }
        } catch (Exception $ex) {
            throw new Exception('Erro ao recuperar os registros de sucata: '. $ex->getMessage());
        } 
    }       
    
    public static function insertSurcata($marca = null, $qt, $amperagem) {
        try {
            $sql = "INSERT INTO agsbaterias.sucata (marca, quantidade, amperagem, dataDoCadastro)
                    VALUES('$marca', '$qt', '$amperagem', now())";
            $query = self::getConection()->prepare($sql);
            $query->execute();
            $cont = $query->rowCount();
            if($cont == 0) {
                throw new Exception('Erro ao inserir o registro');
            }
        } catch (Exception $ex) {
            throw new Exception('Erro ao inserir o registro: '. $ex->getMessage());
        }
    }
    
    public static function updateSucata($marca = null, $qt, $amperagem) {
        try {
            $db = self::getConection();
            $db->beginTransaction();
            $sql = "UPDATE agsbaterias.sucata SET marca = '$marca', quantidade = '$qt', amperagem = '$amperagem', dataDoCadastro = now()";
            $query = $db->prepare($sql);
            $query->execute();
            $cont = $query->rowCount();
            if($cont == 0) {
                $db->rollBack();
                throw new Exception('Erro ao atualizar o registro');
            } else {
                $db->commit();
            }
        } catch (Exception $ex) {
            throw new Exception('Erro atualizar o registro: '. $ex->getMessage());
        }
    }
}