<?php

class VendasController extends LoggedController {
    
    public function beforeRender() {
        $this->Template->setMaster('template_interno_modulos');
    }
    
    public function index() {
        try {  
            $vendas = VendasManager::getAllVendasADinheiro();
            $this->_set('vendas', $vendas);
            $this->_set('tableTitle', 'Controle de Vendas à Dinheiro');
            return $this->_view();            
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/vendas/index');
        }
    }
    
    public function cadastrar_venda() {
        try {  
            if(is_post) {
                $post = $this->_data();                 
                $post->desconto = (isset($post->desconto) && !empty($post->desconto))? $post->desconto: null;                
                VendasManager::insert($post->marca, $post->quantidade, $post->valor, $post->desconto, $post->cliente, $post->formaDePagamento, $this->user->id);                
                $this->_flash('alert alert-success', 'Venda cadastrada com sucesso');
                return $this->_redirect('~/ags_baterias/vendas/index');
            } else {
                $marcas = EstoqueManager::getAll();
                $this->_set('marcas', $marcas);
                return $this->_view();
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/vendas/index');
        }
    }
    
    public function vendas_no_cartao() {
        try {  
            $vendas = VendasManager::getAllVendasNoCartao();
            $this->_set('vendas', $vendas);
            $this->_set('tableTitle', 'Controle de Vendas no Cartão');
            return $this->_view('index');            
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/vendas/index');
        }
    }
}