<?php

class SucataController extends LoggedController {
    
    public function beforeRender() {
        $this->Template->setMaster('template_interno_modulos');
    }
    
    public function index() {
        try {  
            $sucatas = EstoqueManager::getAllSucatas();
            $this->_set('sucatas', $sucatas);
            return $this->_view();            
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/sucata/index');
        }
    }
    
    public function deletar_sucata($idSucata) {
        try {  
            EstoqueManager::deleteSucataById($idSucata);
            $this->_flash('alert alert-success', 'Registro excluído com sucesso');
            return $this->_redirect('~/ags_baterias/sucata/index');    
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/sucata/index');
        }
    }
    
    public function cadastrar_sucata() {
        try {                          
            if (is_post) {
                $post = $this->_data();
                $post->marca = (isset($post->marca) && !empty($post->marca))? $post->marca: null;
                EstoqueManager::insertSurcata($post->marca, $post->quantidade, $post->amperagem);
                $this->_flash('alert alert-success', 'Registro inserido com sucesso');
                return $this->_redirect('~/ags_baterias/sucata/index');
            } else {
                return $this->_view();
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/sucata/index');
        }
    }
    
    public function editar_sucata($idSucata) {
        try {                          
            if (is_post) {
                $post = $this->_data();
                $post->marca = (isset($post->marca) && !empty($post->marca))? $post->marca: null;
                EstoqueManager::updateSucata($post->marca, $post->quantidade, $post->amperagem);
                $this->_flash('alert alert-success', 'Registro inserido com sucesso');
                return $this->_redirect('~/ags_baterias/sucata/index');
            } else {
                $sucata = EstoqueManager::getSucataById($idSucata);
                $this->_set('sucata', $sucata);                
                return $this->_view('cadastrar_sucata');
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/sucata/index');
        }
    }
}