<?php

class HomeController extends LoggedController {
    
    public function beforeRender() {
        $this->Template->setMaster('template_interno_modulos');
    }
    
    public function index() {
        try {                          
            $estoque = EstoqueManager::getAll();                
            $this->_set('estoque', $estoque);
            return $this->_view();            
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/home/index');
        }
    }
    
    public function deletar_estoque($idEstoque) {
        try {              
            $result = EstoqueManager::deleteById($idEstoque);
            $this->_flash('alert alert-success', 'Registro excluído com sucesso');
            return $this->_redirect('~/ags_baterias/home/index');
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/home/index');
        }
    }
    
    public function cadastrar_estoque() {
        try {              
            if (is_post) {
                $post = $this->_data();
                EstoqueManager::insert($post->marca, $post->amperagem, $post->quantidade, $post->valor);
                $this->_flash('alert alert-success', 'Registro inserido com sucesso');
                return $this->_redirect('~/ags_baterias/home/index');
            } else {
                return $this->_view();
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/home/index');
        }
    }
    
    public function editar_estoque($idEstoque) {
        try {              
            if (is_post) {
                $post = $this->_data();
                EstoqueManager::updateById($idEstoque, $post->marca, $post->amperagem, $post->quantidade, $post->valor);
                $this->_flash('alert alert-success', 'Registro alterado com sucesso');
                return $this->_redirect('~/ags_baterias/home/index');
            } else {
                $estoque = EstoqueManager::getById($idEstoque);                
                $this->_set('estoque', $estoque);
                return $this->_view('cadastrar_estoque');
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/ags_baterias/home/index');
        }
    }
}
