<script>
    $(document).ready(function() {
       $('#fixed-second-bar').attr('style', 'display: none'); 
    });
</script>
<style>
    #row-one {
        margin-top: 100px;
    }        
</style>
<div id="row-one" class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
        <h2 class="titulo">Cadastrar Nova Venda</h2>
        <form id="formCadastrar" name="formCadastrar" method="post" action="~/ags_baterias/vendas/cadastrar_venda">
            <div class="form-group">
                <label for="marca">Marca: </label>
                <select id="marca" name="marca" class="form-control" required>
                    <option value="">Selecione a marca</option>
                    <?php if(isset($marcas) && !empty($marcas)): ?>
                        <?php foreach ($marcas as $m): ?>
                            <option value="<?= $m->id ?>"><?= $m->marca." ($m->amperagem) Ampéres" ?></option>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <?php endif; ?>
                </select>
            </div>            
            <div class="form-group">
                <label for="quantidade">Quantidade</label>
                <input type="number" min="1" class="form-control" id="quantidade" name="quantidade" placeholder="Digite a quantidade" required>
            </div>                                    
            <div class="form-group">
                <label for="desconto">Desconto</label>
                <input type="number" step="any" min="0" class="form-control" id="desconto" name="desconto" placeholder="Digite o valor do desconto">
            </div>                                    
            <div class="form-group">
                <label for="formaDePagamento">Forma de Pagamento</label>
                <div class="radio">
                    <label class="radio-inline">
                        <input type="radio" id="dinheiro" value="dinheiro" name="formaDePagamento" required checked> Dinheiro
                    </label>
                    <label class="radio-inline">
                        <input type="radio" id="cartao" value="cartao" name="formaDePagamento"> Cartão
                    </label>    
                </div>
            </div>   
            <div class="form-group">
                <label for="cliente">Cliente</label>
                <input type="text"  class="form-control" id="cliente" name="cliente" placeholder="Digite o nome de cliente" required>
            </div> 
            <div class="form-group">
                <label for="valor" style="background-color: green; color: #fff; border-radius: 2px;">Valor a Pagar (Em reais R$)</label>
                <input type="number"  step="any" min="1" class="form-control" id="valor" name="valor"  placeholder="" required readonly>
            </div>            
            <hr>             
            <button type="submit" id="btnSalvar" name="btnSalvar" class="btn btn-success pull-right"><i class="fa fa-file"></i> Salvar</button>
            <a href="~/ags_baterias/vendas/index"  class="btn btn-danger pull-right" style="margin-right: 8px;"><i class="fa fa-ban"></i> Cancelar</a>
        </form>  
    </div>
</div>    
<!--Modal confirmação de cadastro de venda -->
<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px " >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="closeInfo" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Cadastrar Venda</h4>
            </div>

            <div class="modal-body" id="divExcluir">
                <p>Tem certeza que deseja cadastrar esta venda?</p>
            </div>         
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Não</button>
                <button   id="btnConfirma" name="btnConfirma" class="btn btn-success"><i class="fa fa-check"></i> Sim</button>
            </div>            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function() {
        <?php if(isset($marcas) && !empty($marcas)): ?>
            var marcas = [];    
            var valorMarca;
            var valorFinal;
            <?php foreach ($marcas as $m): ?>
                var item = {id: <?= $m->id ?>, quantidade: <?= $m->quantidade ?>, valor: <?= $m->valor ?>, marca: "<?= $m->marca ?>", amperagem: "<?= $m->amperagem ?>"};
                marcas[<?= $m->id ?>] = (item);
            <?php endforeach; ?>
        <?php endif; ?>    
        $('#marca').change(function() {
            var id = $('#marca option:selected').val();
            if(id != "") {                
                $('#quantidade').attr('max', marcas[id].quantidade);
                $('#quantidade').val(1);
                valorMarca = marcas[id].valor;
                $('#valor').val(valorMarca);
                $('#desconto').attr('max', valorMarca);
                $('#desconto').val('');                
            }            
        });       
        $('#quantidade').change(function() {
           valorFinal =  valorMarca * $('#quantidade').val();
           $('#valor').val(valorFinal);           
           $('#desconto').attr('max', valorFinal);
        });        
//        $('#desconto').keyup(function () {            
//            var valorDesconto = $('#desconto').val();            
//            if(valorDesconto != '') {
//                $('#valor').val(valorFinal - valorDesconto);
//            } else {
//                $('#valor').val(valorFinal);
//            }
//        });
//        $('#desconto').change(function () {            
//            var valorDesconto = $('#desconto').val();            
//            if(valorDesconto != '') {                
//                $('#valor').val(valorFinal - valorDesconto);
//            } else {
//                $('#valor').val(valorFinal);
//            }
//        });
    });
</script>