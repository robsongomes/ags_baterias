<style>         
    .deleteItem {
        cursor: pointer;
    }
</style>
<div class="container">
    <div id="row-one" class="row table-dados">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a href="~/ags_baterias/vendas/cadastrar_venda" class="btn btn-primary pull-right tooltype" title="Clique para cadastrar uma venda" data-placement="left" style="margin-bottom: 8px; margin-top: 20px;"><i class="fa fa-plus-circle fa-lg"></i> Cadastrar Venda</a>
            <table id="tableEstoque" class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th colspan="8" style="text-align: center;"><?= (isset($tableTitle) && !empty($tableTitle))? $tableTitle:'Controle de Vendas' ?></th>
                    </tr>
                    <tr class="active">
                        <th>#</th>
                        <th>Marca</th>
                        <th>Amperagem</th>
                        <th>Quantidade</th>
                        <th>Valor</th>
                        <th>Desconto</th>
                        <th>Data da Venda</th>
                        <th>Cliente</th>                        
                        <th>Vendedor</th>
                    </tr>
                </thead>                
                <tbody>
                    <?php if(isset($vendas) && !empty($vendas)): ?>
                        <?php foreach ($vendas as $key => $e): ?>                            
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= (isset($e->marca) && !empty($e->marca)) ? $e->marca:'--'?></td>
                                <td><?= (isset($e->amperagem) && !empty($e->amperagem)) ? $e->amperagem:'--'?></td>
                                <td><?= (isset($e->quantidade) && !empty($e->quantidade)) ? $e->quantidade:'--'?></td>
                                <td><?= (isset($e->valor) && !empty($e->valor)) ? 'R$ '.number_format(($e->valor), 2, ',', '.'):'--'?></td> 
                                <td><?= (isset($e->desconto) && !empty($e->desconto)) ? 'R$ '.number_format(($e->desconto), 2, ',', '.'):'--'?></td> 
                                <td><?= (isset($e->dataDaVenda) && !empty($e->dataDaVenda)) ? date("d/m/Y", strtotime($e->dataDaVenda)):'--'?></td>
                                <td><?= (isset($e->cliente) && !empty($e->cliente)) ? $e->cliente:'--'?></td>                                
                                <td><?= (isset($e->nomeUsuario) && !empty($e->nomeUsuario)) ? $e->nomeUsuario :'--'?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                            <tr>
                                <td colspan="8">Não há nenhuma venda cadastrada</td>
                            </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--Modal confirmação de remoção de estoque -->
<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px " >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="closeInfo" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Excluir Cadastro</h4>
            </div>

            <div class="modal-body" id="divExcluir">
                <p>Tem certeza que deseja excluir esta venda?</p>
            </div>         
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Não</button>
                <a href="#"  id="btnExcluir" class="btn btn-success"><i class="fa fa-check"></i> Sim</a>
            </div>            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function() {
       $('.deleteItem').click(function() {
         var id = $(this).attr('value'); 
         console.log(id);
         $('#btnExcluir').attr('href', '~/ags_baterias/home/deletar_estoque/'+id+'');
         $('#modalExcluir').modal('show');
       }); 
    });
</script>