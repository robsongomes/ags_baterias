<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">     
        <title><?= (isset($pageTitle) && !empty($pageTitle)) ? $pageTitle : 'AGS Baterias' ?></title>

        <!--Importando a fonte-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:700,100" rel="stylesheet" type="text/css">

        <!--Importando font-awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        
        <!--Animate.css-->
        <link href="~/assets/css/animate.css" rel="stylesheet">
        
        <!--importando boottrap-->
        <link href="~/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="~/assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

        <!--Importando estilo do template-->
        <link rel="stylesheet" type="text/css" href="~/assets/css/template.css">               
                
    </head>
    <body>        
        <!--Importando javascript-->
        <script type="text/javascript" src="~/assets/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="~/assets/bootstrap/js/bootstrap.min.js"></script> 
        
        <!--Importando charts -->
        <!--script type="text/javascript" src="~/assets/chartjs/Chart.js"></script--> 
        
        <nav id="fixed-principal-bar" class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Navegação</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a id="PS" class="navbar-brand"  href="#">
                        AGS
                    </a>
                </div>
                <?php if (1 == 1): ?>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <?php $endereco = $_SERVER ['REQUEST_URI'];	?>
                            <li class="hidden-xs hidden-sm"><a style="height: 60px;" class="tooltype" title="Página inicial" data-placement="bottom" id="PSmenu" href="~/home/index"><i class="fa fa-home fa-lg"></i></a></li>                                                                       
                            <li <?php if(strpos($endereco, 'ags_baterias/home/index')) { ?>class="active"<?php } ?>><a style="height: 60px;" class="tooltype" title="" data-placement="bottom" href="~/ags_baterias/home/index">Estoque <span class="sr-only">(atual)</span></a></li>                                                
                            <li <?php if(strpos($endereco, 'ags_baterias/vendas/index')) { ?>class="active"<?php } ?>><a style="height: 60px;" class="tooltype" title="" data-placement="bottom" href="~/ags_baterias/vendas/index">Vendas à Dinheiro </a></li>
                            <li <?php if(strpos($endereco, 'ags_baterias/vendas/vendas_no_cartao')) { ?>class="active"<?php } ?>><a style="height: 60px;" class="tooltype" title="" data-placement="bottom" href="~/ags_baterias/vendas/vendas_no_cartao">Vendas no Cartão</a></li>  
                            <li <?php if(strpos($endereco, 'ags_baterias/sucata/index')) { ?>class="active"<?php } ?>><a style="height: 60px;" class="tooltype" title="" data-placement="bottom" href="~/ags_baterias/sucata/index">Controle de Sucata</a></li>                                                                                                                          
                            <li <?php if(strpos($endereco, 'dsdsadsa')) { ?>class="active"<?php } ?>><a style="height: 60px;" class="tooltype" title="" data-placement="bottom" href="#">Fluxo de Caixa</a></li>                                                
                            
                            <!--Específica para dis. Móveis. -->
                            <li class="hidden-lg hidden-md"><a  title="Página inicial" data-placement="bottom" id="PSmenu" href="~/home/index"><i class="fa fa-home fa-lg"></i> Home </a></li>                                                                       
                            <li class="hidden-lg hidden-md"><a title="Clique para sair do sistema" data-placement="bottom" id="logout" href="~/home/logout"><i class="fa fa-power-off fa-lg"></i> Sair</a></li>
                        </ul>
                        <div class="pull-right hidden-xs hidden-sm">
                            <a class="tooltype" title="Clique para sair do sistema" data-placement="left" id="logout" href="~/home/logout"><i class="fa fa-power-off" style="font-size: 2.5em; margin-top: 17px;"></i></a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </nav>                                             
        <?= flash ?>
        <?= content ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.tooltype').tooltip();
                $('.alert alert-danger alert-dismissible').add("button");  //função para criar caixas de texto ao passar o cursor do mouse.

                /* essas são responsáveis por fazer as mensagens flash desaparecerem suavemente após
                 * determinado período de tempo.
                 */
                $(".alert-success").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-success").alert('close');
                });
                $(".alert-danger").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $(".alert-warning").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
            });
        </script>
    </body>
</html>