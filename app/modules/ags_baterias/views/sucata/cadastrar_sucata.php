<script>
    $(document).ready(function() {
       $('#fixed-second-bar').attr('style', 'display: none'); 
    });
</script>
<style>
    #row-one {
        margin-top: 100px;
    }        
</style>
<div id="row-one" class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
        <h2 class="titulo">Cadastrar Nova Sucata</h2>
        <form id="formCadastrar" name="formCadastrar" method="post" action="~/ags_baterias/sucata/<?= (isset($sucata) && !empty($sucata))? "editar_sucata/$sucata->id":'cadastrar_sucata' ?>">
            <div class="form-group">
                <label for="marca">Marca</label>
                <input type="text" class="form-control" id="marca" name="marca" value="<?= (isset($sucata->marca) && !empty($sucata->marca))? $sucata->marca:'' ?>" placeholder="Digite a marca">
            </div>
            <div class="form-group">
                <label for="amperagem">Amperagem</label>
                <input type="number" min="0" class="form-control" id="amperagem" name="amperagem" value="<?= (isset($sucata->amperagem) && !empty($sucata->amperagem))? $sucata->amperagem:'' ?>" placeholder="Digite a amperagem" required autofocus>
            </div>            
            <div class="form-group">
                <label for="quantidade">Quantidade</label>
                <input type="number" min="1" class="form-control" id="quantidade" name="quantidade" value="<?= (isset($sucata->quantidade) && !empty($sucata->quantidade))? $sucata->quantidade:'' ?>" placeholder="Digite a quantidade" required>
            </div>                                
            <hr>             
            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-file"></i> Salvar</button>
            <a href="~/ags_baterias/sucata/index"  class="btn btn-danger pull-right" style="margin-right: 8px;"><i class="fa fa-ban"></i> Cancelar</a>
        </form>  
    </div>
</div>    