<style>         
    .deleteItem {
        cursor: pointer;
    }
</style>
<div class="container">
    <div id="row-one" class="row table-dados">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a href="~/ags_baterias/sucata/cadastrar_sucata" class="btn btn-primary pull-right tooltype" title="Clique para cadastrar uma nova sucata" data-placement="left" style="margin-bottom: 8px; margin-top: 20px;"><i class="fa fa-plus-circle fa-lg"></i> Cadastrar Sucata</a>
            <table id="tableEstoque" class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th colspan="7" style="text-align: center;">Controle de Estoque de Sucata</th>
                    </tr>
                    <tr class="active">
                        <th style="width: 5%;">#</th>
                        <th style="width: 40%;">Marca</th>
                        <th style="width: 15%;">Amperagem</th>
                        <th style="width: 15%;">Quantidade</th>
                        <th style="width: 15%;">Última Alteração</th>
                        <th colspan="2" style="width: 10%;">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($sucatas) && !empty($sucatas)): ?>
                        <?php foreach ($sucatas as $key => $e): ?>                            
                            <tr>
                                <td><?= $key + 1 ?></td>
                                <td><?= (isset($e->marca) && !empty($e->marca)) ? $e->marca:'--'?></td>
                                <td><?= (isset($e->amperagem) && !empty($e->amperagem)) ? $e->amperagem:'--'?></td>
                                <td><?= (isset($e->quantidade) && !empty($e->quantidade)) ? $e->quantidade:'--'?></td>
                                <td><?= (isset($e->dataDoCadastro) && !empty($e->dataDoCadastro)) ? date("d/m/Y", strtotime($e->dataDoCadastro)):'--'?></td>                                
                                <td>
                                    <a class="btn btn-success btn-xs tooltype" title="Clique para editar o registro" href="~/ags_baterias/sucata/editar_sucata/<?= $e->id ?>"><i class="fa fa-pencil fa-lg"></i></a>                                    
                                </td>
                                <td>
                                    <button class="btn btn-danger btn-xs tooltype" title="Clique para excluir o registro"><i id="<?= $e->id ?>" value="<?= $e->id ?>" class="fa fa-trash-o fa-lg deleteItem"></i></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                            <tr>
                                <td colspan="6">Não há nenhum item cadastrado em sucata</td>
                            </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--Modal confirmação de remoção de sucata -->
<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px " >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"  id="closeInfo" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Excluir Sucata</h4>
            </div>

            <div class="modal-body" id="divExcluir">
                <p>Tem certeza que deseja excluir este registro?</p>
            </div>         
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Não</button>
                <a href="#"  id="btnExcluir" class="btn btn-success"><i class="fa fa-check"></i> Sim</a>
            </div>            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function() {
       $('.deleteItem').click(function() {
         var id = $(this).attr('value'); 
         console.log(id);
         $('#btnExcluir').attr('href', '~/ags_baterias/sucata/deletar_sucata/'+id+'');
         $('#modalExcluir').modal('show');
       }); 
    });
</script>