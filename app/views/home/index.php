<style>
    #row-one {
        margin-top: 90px;
    }
    .titulo {
        text-align: center;
    }
    #hrLinha {
        height: 500px;
        width: 3px;
        background-color: rgb(221, 221, 221);
    }        
    .grafico {
        margin-left: 5%;
    }
    .row-seguinte {
        margin-top: 30px;
    }
    .btn {
        margin-top: 6px;
    }

    p {
        margin-top: 25px;
        text-align: center;
        font-size: 15px !important;
    }
</style>

<div class="container">    
    <div id="row-one" class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 box-project animated fadeInDown" style="border-left: 5px solid #419641 !important;">
            <a href="~/ags_baterias/home/index" class="btn btn-success btn-lg pull-right"><i class="fa fa-arrow-circle-right"></i> Acesse</a>
            <h1 class="titulo">AGS Baterias</h1>            
        </div>                
    </div>
    <div class="row row-seguinte">
        <div class="col-xs-12 col-sm-12 col-md-12 box-project animated fadeInDown" style="border-left: 5px solid #eb9316 !important;">
            <a href="#" class="btn btn-warning btn-lg pull-right"><i class="fa fa-arrow-circle-right"></i> Acesse</a>
            <h1 class="titulo">AGS Lava Jato</h1>            
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {        
    });
</script>