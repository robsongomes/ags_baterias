<link href="~/assets/css/logon.css" rel="stylesheet" type="text/css">
<div class="container">

    <div class="row" style="margin-top:70px">
        <div id="div-logon" class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" method="post" action="~/home/logon">
                <fieldset>
                    <h2>Efetuar logon</h2>
                    <hr class="colorgraph">
                    <div class="form-group">
                        <input type="text" name="login" id="email" class="form-control input-lg" placeholder="Nome de usuário" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="password" name="senha" id="password" class="form-control input-lg" placeholder="Senha" required>
                    </div>                    
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <a href="javascript:window.history.back();" class="btn btn-lg btn-warning btn-block tooltype" title="Voltar ao site da AGS Baterias"><i class="fa fa-undo fa-lg"></i> Voltar ao Site</a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">                            
                            <button type="submit" class="btn btn-lg btn-success btn-block tooltype" title="Entrar no Sistema"><i class="fa fa-sign-in fa-lg"></i> Entrar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>
<script type="text/javascript" src="~/assets/js/logon.js"></script>
    