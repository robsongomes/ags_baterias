<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
      
        <title><?= (isset($pageTitle) && !empty($pageTitle)) ? $pageTitle : 'AGS Baterias' ?></title>

        <!--Importando a fonte-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:700,100" rel="stylesheet" type="text/css">

        <!--Importando font-awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!--importando boottrap-->
        <link href="~/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="~/assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

        <!--Importando estilo do template-->
        <link rel="stylesheet" type="text/css" href="~/assets/css/template.css">               
    </head>
    <body>        
        <!--Importando javascript-->
        <script type="text/javascript" src="~/assets/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="~/assets/bootstrap/js/bootstrap.min.js"></script>            
        
        <?= flash ?>
        <?= content ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.tooltype').tooltip();
                $('.alert alert-danger alert-dismissible').add("button");  //função para criar caixas de texto ao passar o cursor do mouse.

                /* essas são responsáveis por fazer as mensagens flash desaparecerem suavemente após
                 * determinado período de tempo.
                 */
                $(".alert-success").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-success").alert('close');
                });
                $(".alert-danger").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
                $(".alert-warning").fadeTo(5000, 1000).slideUp(500, function() {
                    $(".alert-danger").alert('close');
                });
            });
        </script>
    </body>
</html>