<?php

class LoggedController extends Controller {

    protected $user;
    public $page_title = "AGS Baterias";
    
    public function __construct() {
        if (!Auth::user() || !Auth::isLogged()) {
            $this->_flash('alert alert-warning', 'Você precisa entrar no sistema para usar esta funcionalidade');
            return $this->_redirect('~/home/logon');
        }
        $this->user = Auth::user();            
        $this->_set('page_title', $this->page_title);
        $this->_set('usuario', $this->user);                
    }
    
    public function limparDadosUsuario() {
        $this->user = null;
        $this->_set('usuario', null);
    }

//    public function getAvatar($caminhoAvatar) {
//        if ($caminhoAvatar) {
//            $caminho = App::ROOT . "app/wwwroot/uploads/photos/" . $caminhoAvatar;
//            if (file_exists($caminho)) {
//                return "~/uploads/photos/" . $caminhoAvatar;
//            }
//        }
//        return "~/uploads/photos/avatar.png";
//    }

//    public function beforeRender() {
//        $this->Template->setMaster('template');
//    }
//    

    public function verificaPermissao() {
        $permissoes = func_get_args();
        $permissoes[] = "Administradores";
        $permissoes[] = "NAE";
        try {            
            call_user_func_array('Auth::allow', $permissoes);            
            return true;
        } catch (Exception $e) {
            $this->_flash('danger', $e->getMessage());
            $this->_redirect("~/home/index");
        }
    }    

}
?>
