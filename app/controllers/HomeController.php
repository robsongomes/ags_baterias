<?php
class HomeController extends Controller
{
//    public function beforeRender() {        
//        $this->Template->setMaster('template_interno');
//    }
    
    public function index() {
        try {  
            $this->Template->setMaster('template_interno');
            if(Auth::isLogged()) {                    
                return $this->_view();
            } else {                
                return $this->_redirect('~/home/logon');
            }
        } catch (Exception $ex) {
            $this->_flash('alert alert-danger', $ex->getMessage());
            return $this->_redirect('~/home/logon');
        }
    }    
    public function logon() {                          
        try {
            if (Auth::isLogged()) {
                return $this->_redirect('~/home/index');
            }
            if(is_post) {                    
                $post = $this->_data();
                $usuario = UsuarioManager::getByLoginAndSenha($post->login, $post->senha);
                if($usuario->nomeDoPapel == 'Administrador') {
                    Auth::set('Administrador');
                } elseif($usuario->nomeDoPapel == 'Normal') {
                    Auth::set('Normal');
                } else {
                    $this->_flash('alert alert-warning', 'Você não tem permissão para acessar a página solicitada');
                    return $this->_redirect('~/home/logon');
                }
                Auth::login($usuario);                    
                return $this->_redirect('~/home/index');
            } else {                    
                return $this->_view();
            }            
        } catch (Exception $ex) {
            $this->_flash('alert alert-warning', $ex->getMessage());
            return $this->_redirect('~/home/logon');
        }
    }
    
    public function logout() {        
        if(Auth::isLogged()) {
            Auth::remove('admin');            
            Auth::remove('normal');
            Auth::logout();            
        }
        //LoggedController::limparDadosUsuario();
        return $this->_redirect('~/home/logon');
    }   
}